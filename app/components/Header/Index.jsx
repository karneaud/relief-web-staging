import React from 'react';
import './index.css';

export default function Header() {
    return  (
      <header className="header">
        <nav className="navbar navbar-expand-lg navbar-dark fixed-top primary">
          <a className="navbar-brand" href="/"><img src={`../../public/images/logo.png`} alt="Relief TT Logo"/></a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-links" aria-controls="menuLinks"
            aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="menu-links">
            <a href="https://goo.gl/forms/wJTgmVzmC1YDJSuD2">
              <button className="btn btn-lg btn-warning add-location" type="submit">Add New Location</button>
            </a>
          </div>
        </nav>
      </header>
  );
}
