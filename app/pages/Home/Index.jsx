// Dead simple component for the hello world (hi mom!)

import React from 'react';
import { Helmet } from 'react-helmet';
import Header from '../../components/Header/Index';
import './index.css';

export default function Home() {
  return (
    <section>
      <Header />
      <Helmet>
        <title>Hello World!</title>
      </Helmet>
      <h1 className="hello-world">Hello world!</h1>
      <p style={{ textAlign: 'center' }}>
      This is an ordinary react component.
      </p>
    </section>
  );
}
