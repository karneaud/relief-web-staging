# Relief TT Website

Website front-end for the ReliefTT project using NodeJS + React. The project boilerplate is based on the parcel-react-ssr project by Benoit Tremblay <benoit@reactivestack.com>

# Overview

Simple example of how to do server-rendering. You will not believe how easy it is!

## Using:

* parcel-bundler
* react
* react-router-dom
* react-helmet (SEO)
* react-imported-component (It's there but not used)

## Handy Extras

* gzip compression
* eslint
* [favicon example](server/index.js#16)

## How to run

* `npm run dev` - Run the development server with hot reload but no server-rendering. project dev runs on parcel port 3000
* `npm run build` - Build for production, required before running. set env PUBLIC_URL to site url before build.
* `npm start` - Start the production server. set env PORT to preferred port.

## How does it work?

To do proper server-rendering, the code is bundled in two version: one for the browser and one for Node.js.

The browser version is in the `dist/client` folder and the Node.js version is in `dist/server`. However, they both share the same public path for their files: `/dist` and it points to the browser version. Go read the code, it's pretty straightforward!

## Read the code!

1. [package.json](package.json) - Start by reading the `scripts` section to understand how the build process works. Yes it's that dead simple!
1. [app/index.html](app/index.html) - Your only HTML file acting as a template
1. [app/client.js](app/client.js) - Entry point for your browser version
1. [app/App.jsx](app/App.jsx) - Your main application component shared between your browser and Node.js version
1. [server/index.js](server/index.js) - Entry point for your Node.js version
1. [server/middleware.js](server/middleware.js) - Middleware taking care of server-rendering
1. [server/generateHtml.js](server/generateHtml.js) - Generate the HTML using `index.html` as the template with cheerio

## Constructing/ Contributing content
```
|-- app
|	|-- components
|	|	|-- Header
|	|		|-- Index.jsx
|	|		|-- index.css
|	|-- pages
|	|	|-- Home
|	|		|-- Index.jsx
|	|		|-- index.css
|	|-- favicon.ico
|	|-- client.jsx
|	|-- App.jsx
|	|-- index.html
|-- dist (or build)
|-- public (static assets)
|		|-- css
|		|	|-- styles.css (global styles)
|		|-- images
|-- node_modules
|-- README.md
|-- package.json
|-- .gitignore
```
* The index.html is the template file from which will have the global markup.
* Pages will go into the "pages" folder with a a naming convention (Page Name) / Index.jsx, index.css.
* Components will go into the components folder with a naming convention (Component Name) / Index.jsx, index.css.
* Static assets if not a component can go into the public folder where it will be copied over on build.

## TODO

1. Add menu component.
2. Define other content pages as necessary.
3. Copy over site styles.
3. Add map/ search as dependency component.
